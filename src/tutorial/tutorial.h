/**
 * tutorial.h
 *
 *  Created on: Aug 22, 2019
 *      Author: consultit
 */

#ifndef TUTORIAL_H_
#define TUTORIAL_H_

#include <ctime>

extern double My_variable;

int fact(int n);
int my_mod(int x, int y);
char* get_time();

#endif /* TUTORIAL_H_ */
